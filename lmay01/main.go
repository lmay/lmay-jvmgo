package main

import "fmt"

/**
 * @author lmay.Zhou
 * @date 
 * @qq 379839355
 * @blog: www.lmaye.com
 * @email Java_zlm@163.com
 */

 func main() {
 	cmd := parseCmd()
 	if cmd.versionFlag {
		fmt.Print("version 0.0.1")
	} else if cmd.helpFlag || cmd.class == "" {
		printUsage()
	} else {
		startJVM(cmd)
	}
 }